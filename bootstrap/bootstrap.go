package bootstrap

import (
	"fmt"
	"gitee.com/lmuiotctf/chatGpt_wechat/tree/master/handlers"
	"gitee.com/lmuiotctf/chatGpt_wechat/tree/master/pkg/logger"
	"github.com/eatmoreapple/openwechat"
    "time"
	"strconv"
    // "strings"
)

func sendMessageToGroup(gf *openwechat.Group) {
	// for {
	// 	now := time.Now()
	// 	t := time.Date(now.Year(), now.Month(), now.Day(), 6, 20, 0, 0, now.Location())
	// 	timer := time.NewTimer(now.Sub(t))
	// 	<-timer.C
	// 	sendMsg(gf)
	// 	break
	// }
	for {
		now := time.Now()
		t := time.Date(now.Year(), now.Month(), now.Day(), 8, 0, 0, 0, now.Location())
		t1 := time.Date(now.Year(), now.Month(), now.Day(), 9, 0, 0, 0, now.Location())
		t2 := time.Date(now.Year(), now.Month(), now.Day(), 10, 0, 0, 0, now.Location())
		t3 := time.Date(now.Year(), now.Month(), now.Day(), 18, 54, 0, 0, now.Location())
		t4 := time.Date(now.Year(), now.Month(), now.Day(), 22, 0, 0, 0, now.Location())
		if t.Equal(now) {
			sendMsg(gf)
		}
		if t1.Equal(now) {
			sendMsg(gf)
		}
		if t2.Equal(now) {
			sendMsg(gf)
		}
		if t3.Equal(now) {
			sendMsg(gf)
		}
		if t4.Equal(now) {
			sendMsg(gf)
		}
	}
}

func sendMsg(gf *openwechat.Group) {
	now := time.Now()
	tt := time.Date(2023, 7, 31, 0, 0, 0, 0, now.Location())
	ttt := time.Date(2023, 10, 31, 0, 0, 0, 0, now.Location())

	// 年
	fmt.Println(now.Year())
	// 月
	fmt.Println(now.Month())
	// 日
	fmt.Println(now.Day())

	fmt.Println(now.Weekday())

	days, err := GetDaysBetween2Date("20060102", tt.Format("20060102"), now.Format("20060102"))
	days1, err1 := GetDaysBetween2Date("20060102", ttt.Format("20060102"), now.Format("20060102"))
	if err == nil {
		fmt.Println(days)
	}
	if err1 == nil {
		fmt.Println(days1)
	}

	s3 := "离吃我还有 " + strconv.Itoa(days)
	s4 := "离小高收房还有 " + strconv.Itoa(days1)
	gf.SendText(
		"汪 汪 汪 大家好，我叫迪迦，我是你们的智能小助手~ \n\n" +
		"现在时间：" + now.Format("2006-01-02 15:04:05") + "\n" +
		"吃我时间：2023-07-31 00:00:00\n" +
		s3 + " 天" +
		"[Sob][Sob][Sob][Sob][Sob]\n\n" +
		"小高收房时间：2023-10-31\n" +
		s4 + " 天")

	fmt.Println(
		"汪 汪 汪 大家好，我叫迪迦，我是你们的智能小助手~ \n" +
		"现在时间：" + now.Format("2006-01-02 15:04:05") + "\n" +
		"吃我时间：2023-07-31 00:00:00\n" +
		s3 + "天" +
		"[Sob][Sob][Sob][Sob][Sob]")
}

func GetDaysBetween2Date(format, date1Str, date2Str string) (int, error) {
	// 将字符串转化为Time格式
  date1, err := time.ParseInLocation(format, date1Str, time.Local)
  if err != nil {
	  return 0, err
  }
	// 将字符串转化为Time格式
  date2, err := time.ParseInLocation(format, date2Str, time.Local)
  if err != nil {
	  return 0, err
  }
  //计算相差天数
  return int(date1.Sub(date2).Hours() / 24), nil
}

func Run() {
	//bot := openwechat.DefaultBot()
	bot := openwechat.DefaultBot(openwechat.Desktop) // 桌面模式，上面登录不上的可以尝试切换这种模式

	// 注册消息处理函数
	handler, err := handlers.NewHandler()
	if err != nil {
		logger.Danger("register error: %v", err)
		return
	}
	bot.MessageHandler = handler

	// 注册登陆二维码回调
	bot.UUIDCallback = handlers.QrCodeCallBack

	// 创建热存储容器对象
	reloadStorage := openwechat.NewJsonFileHotReloadStorage("storage.json")

	// 执行热登录
	err = bot.HotLogin(reloadStorage, true)
	if err != nil {
		logger.Warning(fmt.Sprintf("login error: %v ", err))
		return
	}

    	// 获取登陆的用户
	self, err := bot.GetCurrentUser()
	if err != nil {
		fmt.Println(err)
		return
	}

	// 获取所有的群组
	groups, err := self.Groups()
	fmt.Println(groups, err)

    sanxiang := groups.SearchByNickName(1, "三湘印象·我们")
	// sanxiangGroup := sanxiang.First()
	if sanxiang.Count() > 0 {
		//go sendMessageToGroup(sanxiangGroup)
	}

	// bot.MessageHandler = func (msg *openwechat.Message) {
	// 	if msg.IsText() && msg.Content == "ping" {
	// 		msg.ReplyText("汪 汪 汪")
	// 	}
	// 	if strings.Contains(msg.Content, "我今年旺不旺") {
	// 		msg.ReplyText("旺 旺 旺")
	// 	}
	// 	if strings.Contains(msg.Content, "交房时间") {
	// 		sendMsg(sanxiangGroup)
	// 	}
	// 	if msg.IsJoinGroup() {
	// 		// msg.ReplyText("汪 汪 汪 🎉🎉🎉🎉🎉🎉欢迎新朋友加入，我叫迪迦，我是你们的智能小助手~")
	// 		sanxiangGroup.SendText("汪 汪 汪 🎉🎉🎉🎉🎉🎉欢迎新朋友加入，我叫迪迦，我是你们的智能小助手~")
	// 		// sender, err := msg.SenderInGroup()
	// 		// fmt.Println(sender, err)
	// 	}
	// 	if strings.Contains(msg.Content, "新闻早读：每日一分钟，尽知天下事。") {
	// 		sanxiangGroup.SendText(strings.Split(msg.Content, "[發]分销时代")[0])
	// 		sendMsg(sanxiangGroup)
	// 	}
	// }

	// 阻塞主goroutine, 直到发生异常或者用户主动退出
	bot.Block()
}
